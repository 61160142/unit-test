/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jobproject;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.time.LocalDate;
import java.time.Month;

/**
 *
 * @author 61160142
 */
public class JobServiceTest {
    
    public JobServiceTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }

    @org.junit.jupiter.api.Test
    public void testCheckEnableTimeTodayIsBetweenStartTimeAndEndTime() {
        System.out.println("checkEnableTime");
        // Arrange
        LocalDate startTime = LocalDate.of(2021, 1, 31);
        LocalDate endTime = LocalDate.of(2021, 2, 5);
        LocalDate today = LocalDate.of(2021, 2, 3);
        boolean expResult = true;
        // Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        // Assert
        assertEquals(expResult,result);
        // TODO review the generated test code and remove the default call to fail.
    }
    public void testCheckEnableTimeTodayIsBeforeStartTime() {
        System.out.println("checkEnableTime");
        // Arrange
        LocalDate startTime = LocalDate.of(2021, 1, 31);
        LocalDate endTime = LocalDate.of(2021, 2, 5);
        LocalDate today = LocalDate.of(2021, 1, 30);
        boolean expResult = false;
        // Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        // Assert
        assertEquals(expResult,result);
        // TODO review the generated test code and remove the default call to fail.
    }
    public void testCheckEnableTimeTodayIsAfterEndTime() {
        System.out.println("checkEnableTime");
        // Arrange
        LocalDate startTime = LocalDate.of(2021, 1, 31);
        LocalDate endTime = LocalDate.of(2021, 2, 5);
        LocalDate today = LocalDate.of(2021, 2, 6);
        boolean expResult = false;
        // Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        // Assert
        assertEquals(expResult,result);
        // TODO review the generated test code and remove the default call to fail.
    }
    public void testCheckEnableTimeTodayIsEqualStartTime() {
        System.out.println("checkEnableTime");
        // Arrange
        LocalDate startTime = LocalDate.of(2021, 1, 31);
        LocalDate endTime = LocalDate.of(2021, 2, 5);
        LocalDate today = LocalDate.of(2021, 1, 31);
        boolean expResult = true;
        // Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        // Assert
        assertEquals(expResult,result);
        // TODO review the generated test code and remove the default call to fail.
    }
    public void testCheckEnableTimeTodayIsEqualEndTime() {
        System.out.println("checkEnableTime");
        // Arrange
        LocalDate startTime = LocalDate.of(2021, 1, 31);
        LocalDate endTime = LocalDate.of(2021, 2, 5);
        LocalDate today = LocalDate.of(2021, 2, 5);
        boolean expResult = true;
        // Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        // Assert
        assertEquals(expResult,result);
        // TODO review the generated test code and remove the default call to fail.
    }
}
